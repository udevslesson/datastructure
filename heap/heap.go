package heap

import "fmt"

type Minheap struct {
	heapArray []int
	size      int
	maxsize   int
}

func NewMinHeap(maxsize int) *Minheap {
	minheap := &Minheap{
		heapArray: []int{},
		size:      0,
		maxsize:   maxsize,
	}
	return minheap
}

func (m *Minheap) Leaf(index int) bool {
	if index >= (m.size/2) && index <= m.size {
		return true
	}
	return false
}

func (m *Minheap) Parent(index int) int {
	return (index - 1) / 2
}

func (m *Minheap) Leftchild(index int) int {
	return 2*index + 1
}

func (m *Minheap) Rightchild(index int) int {
	return 2*index + 2
}

func (m *Minheap) Insert(item int) error {
	if m.size >= m.maxsize {
		return fmt.Errorf("Heal is ful")
	}
	m.heapArray = append(m.heapArray, item)
	m.size++
	m.UpHeapify(m.size - 1)
	return nil
}

func (m *Minheap) Swap(first, second int) {
	temp := m.heapArray[first]
	m.heapArray[first] = m.heapArray[second]
	m.heapArray[second] = temp
}

func (m *Minheap) UpHeapify(index int) {
	for m.heapArray[index] < m.heapArray[m.Parent(index)] {
		m.Swap(index, m.Parent(index))
	}
}

func (m *Minheap) DownHeapify(current int) {
	if m.Leaf(current) {
		return
	}
	smallest := current
	leftChildIndex := m.Leftchild(current)
	rightRightIndex := m.Rightchild(current)
	
	if leftChildIndex < m.size && m.heapArray[leftChildIndex] < m.heapArray[smallest] {
		smallest = leftChildIndex
	}
	if rightRightIndex < m.size && m.heapArray[rightRightIndex] < m.heapArray[smallest] {
		smallest = rightRightIndex
	}
	if smallest != current {
		m.Swap(current, smallest)
		m.DownHeapify(smallest)
	}
	return
}
func (m *Minheap) BuildMinHeap() {
	for index := ((m.size / 2) - 1); index >= 0; index-- {
		m.DownHeapify(index)
	}
}

func (m *Minheap) Remove() int {
	top := m.heapArray[0]
	m.heapArray[0] = m.heapArray[m.size-1]
	m.heapArray = m.heapArray[:(m.size)-1]
	m.size--
	m.DownHeapify(0)
	return top
}
