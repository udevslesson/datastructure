package linkedlist

import (
	"fmt"
)

type Node struct {
	Data int
	next *Node
}


type LinkedList struct {
	length int
	head   *Node
	tail   *Node
}


func (l *LinkedList) Len() int {
	return l.length
}


func (l *LinkedList) PushBack(n *Node) {
	if l.head == nil {
		l.head = n
		l.tail = n
		l.length++
	} else {
		l.tail.next = n
		l.tail = n
		l.length++
	}
}

func (l LinkedList) Display() {
	for l.head != nil {
		fmt.Printf("%v -> ", l.head.Data)
		l.head = l.head.next
	}
	fmt.Println()
}

func (l LinkedList) Front() (int, error) {
	if l.head == nil {
		return 0, fmt.Errorf("Cannot Find Front Value in an Empty linked list")
	}
	return l.head.Data, nil
}

func (l LinkedList) Back() (int, error) {
	if l.head == nil {
		return 0, fmt.Errorf("Cannot Find Front Value in an Empty linked list")
	}
	return l.tail.Data, nil
}

func (l *LinkedList) Reverse() {
	curr := l.head
	l.tail = l.head
	var prev *Node
	for curr != nil {
		temp := curr.next
		curr.next = prev
		prev = curr
		curr = temp
	}
	l.head = prev
}

func (l *LinkedList) Delete(key int) {

	if l.head.Data == key {
		l.head = l.head.next
		l.length--
		return
	}
	var prev *Node = nil
	curr := l.head
	for curr != nil && curr.Data != key {
		prev = curr
		curr = curr.next
	}
	if curr == nil {
		fmt.Println("Key Not found")
		return
	}
	prev.next = curr.next
	l.length--
	fmt.Println("Node Deleted")

}

