package main

import (
	"fmt"
	"udevsLesson/dataStructure/heap"
	lists "udevsLesson/dataStructure/linkedList"
	queues "udevsLesson/dataStructure/queue"
	stacks "udevsLesson/dataStructure/stack"
)

func main() {

	//LinkedList
	list := lists.LinkedList{}
	node1 := &lists.Node{Data: 20}
	node2 := &lists.Node{Data: 30}
	node3 := &lists.Node{Data: 40}
	node4 := &lists.Node{Data: 50}
	node5 := &lists.Node{Data: 70}
	list.PushBack(node1)
	list.PushBack(node2)
	list.PushBack(node3)
	list.PushBack(node4)
	list.PushBack(node5)
	fmt.Println("Length = ", list.Len())
	list.Display()
	list.Delete(40)
	list.Reverse()
	fmt.Println("Length = ", list.Len())
	list.Display()
	front, _ := list.Front()
	back, _ := list.Back()
	fmt.Println("Front = ", front)
	fmt.Println("Back = ", back)

	//Stack

	var stack stacks.Stack 

	stack.Push("this")
	stack.Push("is")
	stack.Push("sparta!!")

	for len(stack) > 0 {
		x, y := stack.Pop()
		if y {
			fmt.Println(x)
		}
	}

	//Heap

	inputArray := []int{6, 5, 3, 7, 2, 8}
	minHeap := heap.NewMinHeap(len(inputArray))
	for i := 0; i < len(inputArray); i++ {
		minHeap.Insert(inputArray[i])
	}
	minHeap.BuildMinHeap()
	for i := 0; i < len(inputArray); i++ {
		fmt.Println(minHeap.Remove())
	}

	//Queue

	var queue []int 

	queue = queues.Enqueue(queue, 10)
	queue = queues.Enqueue(queue, 20)
	queue = queues.Enqueue(queue, 30)

	fmt.Println("Queue:", queue)

	queue = queues.Dequeue(queue)
	fmt.Println("Queue:", queue)

	queue = queues.Enqueue(queue, 40)
	fmt.Println("Queue:", queue)

}
